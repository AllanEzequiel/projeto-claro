
(function ($,jQuery) {
	$('.button-pt').click(function(){
		$('.button-pt').addClass('active');
        $('.button-es').removeClass('active');
	});

	$('.button-es').click(function(){
		$('.button-es').addClass('active');
		$('.button-pt').removeClass('active');
	});

	$('.menuHamburguer').click(function(){
		$(this).toggleClass('active');
		$('body').toggleClass('menuVisible');
	});

	$(function(){
		var current = location.pathname;
		$('.menuNav a').each(function(){
			var $this = $(this);
			if($this.attr('href').indexOf(current) !== -1){
				$this.addClass('active');
			}
		})
	})

})(jQuery);

$(document).on('click', '.anchorButton', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});


// ativa animaÃ§Ãµes
  wow = new WOW(
    {
      boxClass:     'wow',      // animated element css class (default is wow)
      animateClass: 'animated', // animation css class (default is animated)
      offset:       0,          // distance to the element when triggering the animation (default is 0)
      mobile:       false        // trigger animations on mobile devices (true is default)
    }
  );
  wow.init();
